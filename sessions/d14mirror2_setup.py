
from bliss.setup_globals import *

load_script("d14mirror2.py")

print("")
print("Welcome to your new 'd14mirror2' BLISS session !! ")
print("")
print("You can now customize your 'd14mirror2' session by changing files:")
print("   * /d14mirror2_setup.py ")
print("   * /d14mirror2.yml ")
print("   * /scripts/d14mirror2.py ")
print("")