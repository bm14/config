
from bliss.setup_globals import *

load_script("d14oh.py")

print("")
print("Welcome to your new 'd14oh' BLISS session !! ")
print("")
print("You can now customize your 'd14oh' session by changing files:")
print("   * /d14oh_setup.py ")
print("   * /d14oh.yml ")
print("   * /scripts/d14oh.py ")
print("")