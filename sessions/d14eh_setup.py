
from bliss.setup_globals import *

load_script("d14eh.py")

print("")
print("Welcome to your new 'd14eh' BLISS session !! ")
print("")
print("You can now customize your 'd14eh' session by changing files:")
print("   * /d14eh_setup.py ")
print("   * /d14eh.yml ")
print("   * /scripts/d14eh.py ")
print("")