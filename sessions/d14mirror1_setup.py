
from bliss.setup_globals import *

load_script("d14mirror1.py")

print("")
print("Welcome to your new 'd14mirror1' BLISS session !! ")
print("")
print("You can now customize your 'd14mirror1' session by changing files:")
print("   * /d14mirror1_setup.py ")
print("   * /d14mirror1.yml ")
print("   * /scripts/d14mirror1.py ")
print("")